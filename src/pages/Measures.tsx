import React from 'react';
import { IonButton, IonGrid, IonRow, IonCol, IonIcon } from '@ionic/react';
import { GoogleMap, LoadScript } from '@react-google-maps/api';

import { postMedition } from '../helpers/apiRequest';

interface props {
  onFinish: Function;
  meditionData: Object;
}
const Init: React.FC<props> = ({ onFinish, meditionData }) => {
  const mapStyles = {
    height: '80vh',
    width: '100%',
  };

  const coordenadas = {
    coordenada1: {
      lat: 14.6263005,
      lng: -90.5275799,
    },
    coordenada2: {
      lat: 13.826305,
      lng: -70.5085799,
    },
    coordenada3: {
      lat: 12.6263005,
      lng: -95.5205799,
    },
    coordenada4: {
      lat: 13.6262205,
      lng: -70.0205799,
    },
  };

  const defaultCenter = {
    lat: 14.6263005,
    lng: -90.5275799,
  };

  const handleFinish = () => {
    postMedition({
      ...meditionData,
      coordenadas,
    }).then(() => {
      onFinish();
    });
  };

  return (
    <IonGrid className="ion-grid-padding-lg">
      <IonRow>
        <IonCol size="12" className="ion-text-left ion-padding">
          <LoadScript googleMapsApiKey="AIzaSyCb3ljqsqmaP8y7RhU6isw-On1Qx8Igqos">
            <GoogleMap
              mapContainerStyle={mapStyles}
              zoom={13}
              center={defaultCenter}
            />
          </LoadScript>
        </IonCol>
      </IonRow>
      <IonRow>
        <IonButton
          style={{ position: 'absolute' }}
          onClick={handleFinish}
          shape="round"
          color="primary"
        >
          Finalizar
        </IonButton>
      </IonRow>
    </IonGrid>
  );
};

export default Init;
