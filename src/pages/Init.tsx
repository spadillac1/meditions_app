import React from 'react';
import { IonButton, IonGrid, IonRow, IonCol, IonIcon } from '@ionic/react';
import { analyticsOutline } from 'ionicons/icons';

interface props {
  onInit: Function;
}

const Init: React.FC<props> = ({ onInit }) => {
  return (
    <IonGrid className="ion-grid-padding-lg">
      <IonRow>
        <IonCol size="10" className="ion-text-left ion-padding">
          <h1> Calcula el área de los terrenos</h1>
        </IonCol>
      </IonRow>
      <IonRow>
        <IonCol className="ion-text-right">
          <IonButton
            onClick={() => onInit()}
            shape="round"
            color="primary"
          >
            <IonIcon slot="end" icon={analyticsOutline} />
            Comenzar
          </IonButton>
        </IonCol>
      </IonRow>
    </IonGrid>
  );
};

export default Init;
