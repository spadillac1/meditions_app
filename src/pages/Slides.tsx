import React, { useLayoutEffect, useRef, lazy, useState } from 'react';
import { IonSlides, IonSlide, IonContent } from '@ionic/react';
import { IonButton, IonGrid, IonRow, IonCol, IonIcon } from '@ionic/react';

import './Slides.css';

const Init = lazy(() => import('./Init'));
const UserSelections = lazy(() => import('./UserSelections'));
const Measures = lazy(() => import('./Measures'));

const Slides: React.FC = () => {
  const slidesRef = useRef<HTMLIonSlidesElement>(null);
  const [medition, setMedition] = useState({});
  const [slide, setSlide] = useState(1);

  const handleOnNext = (data: object) => {
    setMedition(data);
    slideTo(2);
  };

  const handleStart = () => {
    slideTo(0);
  }

  const slideTo = (slide: number) => {
    slidesRef.current?.lockSwipeToNext(false);
    slidesRef.current?.slideTo(slide);
    slidesRef.current?.lockSwipeToNext(true);
    slidesRef.current?.getActiveIndex().then(setSlide);
  };

  useLayoutEffect(() => {
    slidesRef.current?.lockSwipeToNext(true);
  }, []);

  return (
    <IonContent>
      <IonSlides pager={true} ref={slidesRef}>
        <IonSlide>
          <Init onInit={() => slideTo(1)} />
        </IonSlide>
        <IonSlide>
          <UserSelections slide={slide} onNext={handleOnNext} />
        </IonSlide>
        <IonSlide>
          <Measures onFinish={() => slideTo(4)} meditionData={medition} />
        </IonSlide>
        <IonSlide>
          <IonGrid className="ion-grid-padding-lg">
            <IonRow>
              <IonCol size="10" className="ion-text-left ion-padding">
                <h1>Medicion Agregada</h1>
              </IonCol>
            </IonRow>
            <IonRow>
              <IonCol className="ion-text-right">
                <IonButton onClick={handleStart} shape="round" color="primary">
                  Regresar
                </IonButton>
              </IonCol>
            </IonRow>
          </IonGrid>
        </IonSlide>
      </IonSlides>
    </IonContent>
  );
};

export default Slides;
