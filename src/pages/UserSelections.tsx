import React, { useState, useEffect } from 'react';
import {
  IonButton,
  IonGrid,
  IonRow,
  IonCol,
  IonIcon,
  IonModal,
  IonItem,
  IonItemDivider,
  IonInput,
  IonSelect,
  IonSelectOption,
  IonList,
} from '@ionic/react';
import { arrowForward, closeCircle, addCircle } from 'ionicons/icons';
import { getClients, postClient } from '../helpers/apiRequest';
import { useForm } from '../hooks/useForm';

interface props {
  onNext: Function;
  slide: Number;
}

const UserSelections: React.FC<props> = ({ onNext, slide }) => {
  const [clients, setClients] = useState([]);
  const [showModal, setShowModal] = useState(false);
  const [registryForm, handleRegistryFormChange] = useForm({
    nombre: '',
    apellido: '',
    pais: '',
    telefono: '',
  });
  const [meditionData, handleMeditionChange] = useForm({
    nombreMedicion: '',
    clientName: {},
  });

  const { nombre, apellido, pais, telefono } = registryForm;
  const { nombreMedicion, clientName } = meditionData;

  const handleAdd = () => {
    let save = true;
    for (let property in registryForm) {
      if (registryForm[property] === '') {
        save = false;
      }
    }
    if (save) {
      postClient(registryForm)
        .then((data) => {
          getClients()
            .then((data) => {
              setClients(data);
              setShowModal(false);
            })
            .catch((error) => alert(error.message));
        })
        .catch((error) => alert(error.message));
    }
  };

  const handleNext = () => {
    if (nombreMedicion !== '' && clientName !== {}) {
      onNext(meditionData);
    }
  };

  useEffect(() => {
    if (slide === 1) {
      getClients()
        .then((data) => {
          setClients(data);
        })
        .catch((error) => alert(error.message));
    }
  }, [slide]);

  return (
    <IonGrid className="ion-grid-padding-lg">
      <IonRow>
        <IonCol size="10" className="ion-text-left ion-padding">
          <h2> Información de medición </h2>
        </IonCol>
      </IonRow>
      <IonRow>
        <IonCol>
          <IonInput
            onIonChange={handleMeditionChange}
            autofocus
            type="text"
            color="primary"
            name="nombreMedicion"
            value={nombreMedicion}
            placeholder="Nombre Medición"
          ></IonInput>
        </IonCol>
      </IonRow>
      <IonRow>
        <IonCol>
          <IonSelect
            onIonChange={handleMeditionChange}
            name="clientName"
            value={clientName}
            color="primary"
            placeholder="Seleccionar Cliente"
          >
            {clients.map(({ nombre, apellido }: any) => (
              <IonSelectOption key={nombre} value={`${nombre} ${apellido}`}>
                {nombre} {apellido}
              </IonSelectOption>
            ))}
          </IonSelect>
        </IonCol>
      </IonRow>
      <IonRow>
        <IonCol>
          <IonModal swipeToClose={true} isOpen={showModal}>
            <IonList>
              <IonItemDivider>Nombre</IonItemDivider>
              <IonItem>
                <IonInput
                  onIonChange={handleRegistryFormChange}
                  name="nombre"
                  type="text"
                  placeholder="ej: Mariano"
                  value={nombre}
                ></IonInput>
              </IonItem>
              <IonItemDivider>Apellido</IonItemDivider>
              <IonItem>
                <IonInput
                  onIonChange={handleRegistryFormChange}
                  name="apellido"
                  type="text"
                  placeholder="ej: Gálvez"
                  value={apellido}
                ></IonInput>
              </IonItem>
              <IonItemDivider>País</IonItemDivider>
              <IonItem>
                <IonInput
                  onIonChange={handleRegistryFormChange}
                  name="pais"
                  type="text"
                  placeholder="ej: Guatemala"
                  value={pais}
                ></IonInput>
              </IonItem>
              <IonItemDivider>Teléfono</IonItemDivider>
              <IonItem>
                <IonInput
                  onIonChange={handleRegistryFormChange}
                  name="telefono"
                  type="number"
                  placeholder="ej: 55551555"
                  value={telefono}
                ></IonInput>
              </IonItem>
            </IonList>

            <IonButton color="primary" onClick={handleAdd}>
              <IonIcon slot="start" icon={addCircle} />
              Agregar
            </IonButton>
            <IonButton color="red" onClick={() => setShowModal(false)}>
              <IonIcon slot="start" icon={closeCircle} />
              Cancelar
            </IonButton>
          </IonModal>

          <small onClick={() => setShowModal(true)} color="medium">
            Registrar un cliente
          </small>
        </IonCol>
      </IonRow>

      <IonRow>
        <IonCol className="ion-text-right">
          <IonButton onClick={() => handleNext()} shape="round" color="primary">
            <IonIcon slot="end" icon={arrowForward} />
            Siguiente
          </IonButton>
        </IonCol>
      </IonRow>
    </IonGrid>
  );
};

export default UserSelections;
