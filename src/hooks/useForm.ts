import { useState } from 'react';

export const useForm = (initialState = {}): [any, (e: any) => void] => {
  const [formState, setFormState] = useState(initialState);

  const handleInputChange = ({ target }: any) => {
    setFormState({
      ...formState,
      [target.name]: target.value.toString().trim(),
    });
  };

  return [formState, handleInputChange];
};
