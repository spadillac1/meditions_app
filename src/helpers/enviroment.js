const URI = 'http://localhost:5500/api';

export const collections = {
  clients: `${URI}/clients`,
  meditions: `${URI}/meditions`,
};
