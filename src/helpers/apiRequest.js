import axios from 'axios';
import { collections } from './enviroment';

export const getClients = () => {
  return axios.get(collections.clients).then((res) => {
    if (res.status === 200) {
      console.log(res.data);
      return res.data;
    } else {
      throw new Error('Incorrect Code Number');
    }
  });
};


export const postClient = (data) => {
  return axios.post(collections.clients, data).then((res) => {
    if (res.status === 200) {
      return res.data;
    } else {
      throw new Error('Incorrect Code Number');
    }
  });
};

export const postMedition = (data) => {
  return axios.post(collections.meditions, data).then((res) => {
    if (res.status === 200) {
      return res.data;
    } else {
      throw new Error('Incorrect Code Number');
    }
  });
};
