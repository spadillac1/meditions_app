import React, { Suspense, lazy } from 'react';
import ReactDOM from 'react-dom';
import { IonLoading } from '@ionic/react';

const App = lazy(() => import('./App'));

ReactDOM.render(
  <Suspense
    fallback={<IonLoading isOpen message={'Please wait...'} duration={5000} />}
  >
    <App />
  </Suspense>,
  document.getElementById('root')
);
